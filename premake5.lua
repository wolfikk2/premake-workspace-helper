------------------------------------------------------------------
-- THIS CODE IS NOT INTENDED TO MODIFY ---------------------------
-- PROVIDES AUTOMATIC INCLUDING OF PROJECT BUILD SCRIPTS ---------
------------------------------------------------------------------
WORKSPACE_NAME = "SampleWorkspace"
------------------------------------------------------------------
include "workspace"
------------------------------------------------------------------
print("Processing workspace...")
scripts = os.matchfiles("src/*/build.lua")
for k,v in pairs(scripts) do
	verbosef ("    Executing '%s'", v)
	dofile(v)
end


