-- This is premake-workspace-helper main script for PREMAKE5
-- 5ogether with premake5.lua it contains routines to recursively
-- process other 'build.lua' scripts.
-- There shouldn't be any concrete project setting except for globals.
-- Highly inspired by Marvin Build System

if WORKSPACE_NAME == nil then
	error("You have to set WORKSPACE_NAME global variable.")
end

printf(WORKSPACE_NAME .. " Premake Build System")
printf("Target OS: %s", os.target())

work_dir = os.getcwd()
printf("Directory: %s", work_dir)

-- fix move command token
os.commandTokens.windows.move = function(v)
	v = path.translate(path.normalize(v))

	-- Detect if there's multiple parts to the input, if there is grab the first part else grab the whole thing
	local src = string.match(v, '^".-"') or string.match(v, '^.- ') or v

	-- Strip the trailing space from the second condition so that we don't have a space between src and '\\NUL'
	src = string.match(src, '^.*%S')

	return "IF EXIST " .. src .. " (move /Y " .. v .. ") > nul"
end

-- helper to read current windows sdk version
function os.winSdkVersion()
	local reg_arch = iif( os.is64bit(), "\\Wow6432Node\\", "\\" )
	local sdk_version = os.getWindowsRegistry( "HKLM:SOFTWARE" .. reg_arch .."Microsoft\\Microsoft SDKs\\Windows\\v10.0\\ProductVersion" )
	if sdk_version ~= nil then return sdk_version end
end

-- helper to define project
-- usage: workspace_project { "ProjectName", "Language", "Kind", "Platform" }
function workspace_project(args)
	local n = nil -- name
	local l = nil -- language
	local k = nil -- kind
	local p = nil -- platform
	if type(args) == "string" then
		n = args
	else
		n, l, k, p = table.unpack(args)
		if p ~= nil and type(p) ~= "table" then
			error("workspace_project '".. n .."': Parameter 'Platform' should be table.")
		end
	end

	-- change dir to work env.
	os.chdir(work_dir)

	-- begin premake project
	verbosef("Defining project '%s'", n)
	project(n)
	location("%{wks.location}/%{prj.name}")
	language(l)
	kind(k)

	-- disable build for specific platform
	if p ~= nil and not table.contains(p, os.target()) then
		printf("'%s': not supported on %s", n, os.target())
		flags { "ExcludeFromBuild" }
	end

	-- set target extension
	if k == "SharedLib" then
		system = os.target()
		if system == "windows" then
			targetextension (".dll")
		elseif system == "linux" then
			targetextension (".so")
		elseif system == "macosx" then
			targetextension (".dylib")
		end
	end

	-- define project type as macro
	defines {
		"PROJECT_KIND_" .. string.upper(k)
	}

end

newaction {
	trigger = "clean",
	description = "Cleans-up the workspace",
	execute = function ()
		os.execute "{DELETE} ./workspace/"
		os.execute "{DELETE} ./build"
	end
}

workspace(WORKSPACE_NAME)
	configurations { "Debug", "Release" }
	platforms { "x86", "x64" }
	location "workspace"
	flags { "NoPCH", "MultiProcessorCompile", "NoImplicitLink", "NoMinimalRebuild" }
	targetdir "./build/%{cfg.platform}/%{cfg.buildcfg}"
	debugdir "./build/%{cfg.platform}/%{cfg.buildcfg}"
	characterset ("MBCS")

	files {
		"./src/%{prj.name}/**.c",
		"./src/%{prj.name}/**.cpp",
		"./src/%{prj.name}/**.h",
		"./src/%{prj.name}/**.hpp",
		"./src/%{prj.name}/build.lua",
		"./src/%{prj.name}/rescom.xml",
	}

	includedirs {
		"./src",
		"%{wks.location}/%{prj.name}/include/",
		"./src/%{prj.name}",
		"./src/%{prj.name}/external",
	}

	libdirs {
		"./build/%{cfg.platform}/%{cfg.buildcfg}",
		"./lib/*/%{cfg.platform}",
		"./lib/*/%{cfg.platform}/%{cfg.buildcfg}",
		"./src/%{prj.name}/external/*/lib",
		"./src/%{prj.name}/external/*/lib/%{cfg.platform}",
		"./src/%{prj.name}/external/*/lib/%{cfg.platform}/%{cfg.buildcfg}"
	}

	filter "platforms:x86"
		architecture "x86"
		defines { "ARCH_X86" }

	filter "platforms:x64"
		architecture "x64"
		defines { "ARCH_X64" }

	filter "configurations:Debug*"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Release*"
		defines { "RELEASE" }
		optimize "Speed"
		exceptionhandling "Off"
		flags { "LinkTimeOptimization" }
		omitframepointer "On"

	filter "toolset:msc*"
		buildoptions { "/GF" }
		removedefines { "PURE_ABSTRACT=" }
		defines { "TOOLSET_MSC", "PURE_ABSTRACT=__declspec(novtable)" }
		files {
			"./src/%{prj.name}/**.rc",
			"./src/%{prj.name}/**.natvis"
		}

	filter "toolset:gcc*"
		defines { "TOOLSET_GCC" }
		buildoptions {
			"-w",
			"-fpermissive",
			"-fvisibility=hidden"
		}

	filter "system:windows"
		defines {
			"PLATFORM_WINDOWS",
			"EXPORT=__declspec(dllexport)",
			"FASTCALL",
			"ALIGN(x)=__declspec(align(x))"
		}

		systemversion(os.winSdkVersion() .. ".0")

	filter "system:linux"
		defines {
			"MARVIN_PLATFORM_LINUX",
			"__fastcall",
			"FASTCALL=__attribute__((fastcall))",
			"ALIGN(x)=__attribute__((aligned(x)))",
			"EXPORT=__attribute__ ((visibility (\"default\")))"
		}

	filter "system:macosx"
		defines { "PLATFORM_MACOSX" }

	filter "kind:SharedLib"
		defines { "SHARED_LIBRARY" }

	filter "kind:StaticLib"
		defines { "STATIC_LIBRARY" }

